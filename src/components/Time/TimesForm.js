import React from 'react'
import {Button, TextField, FormControl} from '@material-ui/core'

export default class TimesForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            fundacao: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const time = {
            nome: this.state.nome,
            fundacao: this.state.fundacao
        }

        this.props.handlePost(time)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return ( 
        <FormControl>
            <h2>Times</h2>
            <TextField type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></TextField>
            <TextField type="number" id="fundacao" placeholder="Fundação" value={this.state.fundacao} onChange={this.handleChange}></TextField>
            <p></p>
            <Button onClick={this.handleSubmit} variant="contained" color="primary" size="medium">Salvar</Button>
        </FormControl>
        )
    }
}