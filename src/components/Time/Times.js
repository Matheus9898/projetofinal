import React from 'react'
import Axios from 'axios'
import TimesForm from './TimesForm'
import {Table, TableHead, TableBody, TableCell, TableRow, Box, Button} from '@material-ui/core'

export default class Times extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            times: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8888/times/'
        Axios.get(url).then((response) => {
            console.log(response)
            this.setState({
                times:response.data
            })
        })
     }

    handlePost = (time) => {
        const url = 'http://localhost:8888/times/'
        var request = Axios.post(url, time)

        request.then((response) => {
            this.setState({
                times: [...this.state.times, time]
            })
            this.handleGet()
        })
    }

    handlePut = (id, time) => {
        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:8888/times/' + id
        var request = Axios.put(url, time)

        request.then((response) => {
            this.handleGet()
        })
    }
    
    handleDelete = (id) => {
        const url = 'http://localhost:8888/times/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })           
    }
    
    handleSubmit = (time) => {
        if(this.state.selecionado == null){
            this.handlePost(time)
        } else {
            this.handlePut(this.state.selecionado, time)
        }
    }

    handleSelect = (item) => {
        this.setState({
            selecionado: item._id
        })
    }

    render(){

        var listaTimes = this.state.times.map((item, _id) => {
            
            return(
                    <TableRow flexDirection='column' display="flex" style={{margin: '3rem'}} key={item._id}>
                        <TableCell>{_id + 1}</TableCell>
                        <TableCell>{item.nome}</TableCell>
                        <TableCell>{item.fundacao}</TableCell>
                        <TableCell><Button onClick={() => this.handleSelect(item)} variant="contained" color="primary" size="small">Editar</Button></TableCell>
                        <TableCell><Button onClick={() => this.handleDelete(item._id)} variant="contained" color="primary" size="small">Deletar</Button></TableCell>
                    </TableRow>
            )
        })
        
        return(
            <Box display="flex" flexDirection='column' style={{margin: '3rem'}}>
                <TimesForm handlePost={this.handleSubmit}></TimesForm>
                <Table>                       
                    <TableHead>
                        <TableRow>
                            <TableCell>Quantidade</TableCell>
                            <TableCell>Nome</TableCell>
                            <TableCell>Fundação</TableCell>
                            <TableCell>Editar</TableCell>
                            <TableCell>Excluir</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {listaTimes}
                    </TableBody>
                </Table> 
            </Box>
        )
    }
}