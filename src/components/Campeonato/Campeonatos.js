import React from 'react'
import Axios from 'axios'
import CampeonatosForm from './CampeonatosForm'
import {Table, TableHead, TableBody, TableCell, TableRow, Box, Button} from '@material-ui/core'

export default class Campeonatos extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            campeonatos: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8888/campeonatos/'
        Axios.get(url).then((response) => {
            console.log(response)
            this.setState({
                campeonatos:response.data
            })
        })
     }

    handlePost = (campeonato) => {
        const url = 'http://localhost:8888/campeonatos/'
        var request = Axios.post(url, campeonato)

        request.then((response) => {
            this.setState({
                campeonatos: [...this.state.campeonatos, campeonato]
            })
            this.handleGet()
        })
    }

    handlePut = (id, campeonato) => {
        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:8888/campeonatos/' + id
        var request = Axios.put(url, campeonato)

        request.then((response) => {
            this.handleGet()
        })
    }
    
    handleDelete = (id) => {
        const url = 'http://localhost:8888/campeonatos/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })           
    }
    
    handleSubmit = (campeonato) => {
        if(this.state.selecionado == null){
            this.handlePost(campeonato)
        } else {
            this.handlePut(this.state.selecionado, campeonato)
        }
    }

    handleSelect = (item) => {
        this.setState({
            selecionado: item._id
        })
    }

    render(){

        var listaCampeonatos= this.state.campeonatos.map((item, _id) => {
            
            return(
                    <TableRow flexDirection='column' display="flex" style={{margin: '3rem'}} key={item._id}>
                        <TableCell>{_id + 1}</TableCell>
                        <TableCell>{item.nome}</TableCell>
                        <TableCell>{item.local}</TableCell>
                        <TableCell><Button onClick={() => this.handleSelect(item)} variant="contained" color="primary" size="small">Editar</Button></TableCell>
                        <TableCell><Button onClick={() => this.handleDelete(item._id)} variant="contained" color="primary" size="small">Deletar</Button></TableCell>
                    </TableRow>
            )
        })
        
        return(
            <Box display="flex" flexDirection='column' style={{margin: '3rem'}}>
                <CampeonatosForm handlePost={this.handleSubmit}></CampeonatosForm>
                <Table>                       
                    <TableHead>
                        <TableRow>
                            <TableCell>Quantidade</TableCell>
                            <TableCell>Nome</TableCell>
                            <TableCell>Local</TableCell>
                            <TableCell>Editar</TableCell>
                            <TableCell>Excluir</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {listaCampeonatos}
                    </TableBody>
                </Table> 
            </Box>
        )
    }
}