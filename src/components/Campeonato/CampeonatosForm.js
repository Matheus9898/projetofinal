import React from 'react'
import {Button, TextField, FormControl} from '@material-ui/core'

export default class CampeonatosForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            local: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const campeonato = {
            nome: this.state.nome,
            local: this.state.local
        }

        this.props.handlePost(campeonato)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return ( 
        <FormControl>
            <h2>Campeonatos</h2>
            <TextField type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></TextField>
            <TextField type="text" id="local" placeholder="Local" value={this.state.local} onChange={this.handleChange}></TextField>
            <p></p>
            <Button onClick={this.handleSubmit} variant="contained" color="primary" size="medium">Salvar</Button>
        </FormControl>
        )
    }
}