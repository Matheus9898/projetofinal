import React from 'react'
import Axios from 'axios'
import JogadoresForm from './JogadoresForm'
import {Table, TableHead, TableBody, TableCell, TableRow, Box, Button} from '@material-ui/core'

export default class Jogadores extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            jogadores: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8888/jogadores/'
        Axios.get(url).then((response) => {
            console.log(response)
            this.setState({
                jogadores:response.data
            })
        })
     }

    handlePost = (jogador) => {
        const url = 'http://localhost:8888/jogadores/'
        var request = Axios.post(url, jogador)

        request.then((response) => {
            this.setState({
                jogadores: [...this.state.jogadores, jogador]
            })
            this.handleGet()
        })
    }

    handlePut = (id, jogador) => {
        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:8888/jogadores/' + id
        var request = Axios.put(url, jogador)

        request.then((response) => {
            this.handleGet()
        })
    }
    
    handleDelete = (id) => {
        const url = 'http://localhost:8888/jogadores/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })           
    }
    
    handleSubmit = (jogador) => {
        if(this.state.selecionado == null){
            this.handlePost(jogador)
        } else {
            this.handlePut(this.state.selecionado, jogador)
        }
    }

    handleSelect = (item) => {
        this.setState({
            selecionado: item._id
        })
    }

    render(){

        var listaJogadores = this.state.jogadores.map((item, _id) => {
            
            return(
                    <TableRow flexDirection='column' display="flex" style={{margin: '3rem'}} key={item._id}>
                        
                        <TableCell>{_id + 1}</TableCell>
                        <TableCell>{item.nome}</TableCell>
                        <TableCell>{item.ano_nascimento}</TableCell>
                        <TableCell><Button onClick={() => this.handleSelect(item)} variant="contained" color="primary" size="small">Editar</Button></TableCell>
                        <TableCell><Button onClick={() => this.handleDelete(item._id)} variant="contained" color="primary" size="small">Deletar</Button></TableCell>
                    </TableRow>
            )
        })
        
        return(
            <Box display="flex" flexDirection='column' style={{margin: '3rem'}}>
                <JogadoresForm handlePost={this.handleSubmit}></JogadoresForm>
                <Table>                       
                    <TableHead>
                        <TableRow>
                            <TableCell>Quantidade</TableCell>
                            <TableCell>Nome</TableCell>
                            <TableCell>Ano Nascimento</TableCell>
                            <TableCell>Editar</TableCell>
                            <TableCell>Excluir</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {listaJogadores}
                    </TableBody>
                </Table> 
            </Box>
        )
    }
}