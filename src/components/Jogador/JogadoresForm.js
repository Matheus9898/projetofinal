import React from 'react'
import {Button, TextField, FormControl} from '@material-ui/core'

export default class JogadoresForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            ano_nascimento: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const jogador = {
            nome: this.state.nome,
            ano_nascimento: this.state.ano_nascimento
        }

        this.props.handlePost(jogador)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return ( 
        <FormControl /*onSubmit={this.handleSubmit} style={{margin: '3rem'}}*/>
            <h2>Jogadores</h2>
            <TextField type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></TextField>
            <TextField type="number" id="ano_nascimento" placeholder="Ano de Nascimento" value={this.state.ano_nascimento} onChange={this.handleChange}></TextField>
            <p></p>
            <Button onClick={this.handleSubmit} variant="contained" color="primary" size="medium">Salvar</Button>
        </FormControl>
        )
    }
}