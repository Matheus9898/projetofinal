import React from 'react'
import {Button, Toolbar, AppBar, Typography} from '@material-ui/core'

export default class Menu extends React.Component{
    render(){
        return(
        <AppBar>
        <Toolbar>
            <Typography variant='h3'>Futebol</Typography>
            <Button color="inherit" href="/jogadores">Jogadores</Button>
            <Button color="inherit" href="/campeonatos">Campeonatos</Button>
            <Button color="inherit" href="/estadios">Estadios</Button>
            <Button color="inherit" href="/tecnicos">Tecnicos</Button>
            <Button color="inherit" href="/times">Times</Button>
            </Toolbar>
        </AppBar>
        )
    }
}