import React from 'react'
import Axios from 'axios'
import TecnicosForm from './TecnicosForm'
import {Table, TableHead, TableBody, TableCell, TableRow, Box, Button} from '@material-ui/core'

export default class Tecnicos extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            tecnicos: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8888/tecnicos/'
        Axios.get(url).then((response) => {
            console.log(response)
            this.setState({
                tecnicos:response.data
            })
        })
     }

    handlePost = (tecnico) => {
        const url = 'http://localhost:8888/tecnicos/'
        var request = Axios.post(url, tecnico)

        request.then((response) => {
            this.setState({
                tecnicos: [...this.state.tecnicos, tecnico]
            })
            this.handleGet()
        })
    }

    handlePut = (id, tecnico) => {
        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:8888/tecnicos/' + id
        var request = Axios.put(url, tecnico)

        request.then((response) => {
            this.handleGet()
        })
    }
    
    handleDelete = (id) => {
        const url = 'http://localhost:8888/tecnicos/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })           
    }
    
    handleSubmit = (tecnico) => {
        if(this.state.selecionado == null){
            this.handlePost(tecnico)
        } else {
            this.handlePut(this.state.selecionado, tecnico)
        }
    }

    handleSelect = (item) => {
        this.setState({
            selecionado: item._id
        })
    }

    render(){

        var listaTecnicos = this.state.tecnicos.map((item, _id) => {
            
            return(
                    <TableRow flexDirection='column' display="flex" style={{margin: '3rem'}} key={item._id}>
                        <TableCell>{_id + 1}</TableCell>
                        <TableCell>{item.nome}</TableCell>
                        <TableCell>{item.nascionalidade}</TableCell>
                        <TableCell><Button onClick={() => this.handleSelect(item)} variant="contained" color="primary" size="small">Editar</Button></TableCell>
                        <TableCell><Button onClick={() => this.handleDelete(item._id)} variant="contained" color="primary" size="small">Deletar</Button></TableCell>
                    </TableRow>
            )
        })
        
        return(
            <Box display="flex" flexDirection='column' style={{margin: '3rem'}}>
                <TecnicosForm handlePost={this.handleSubmit}></TecnicosForm>
                <Table>                       
                    <TableHead>
                        <TableRow>
                            <TableCell>Quantidade</TableCell>
                            <TableCell>Nome</TableCell>
                            <TableCell>Nascionalidade</TableCell>
                            <TableCell>Editar</TableCell>
                            <TableCell>Excluir</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {listaTecnicos}
                    </TableBody>
                </Table> 
            </Box>
        )
    }
}