import React from 'react'
import {Button, TextField, FormControl} from '@material-ui/core'

export default class TecnicosForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            nascionalidade: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const tecnico = {
            nome: this.state.nome,
            nascionalidade: this.state.nascionalidade
        }

        this.props.handlePost(tecnico)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return ( 
        <FormControl>
            <h2>Tecnicos</h2>
            <TextField type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></TextField>
            <TextField type="text" id="nascionalidade" placeholder="Nascionalidade" value={this.state.nascionalidade} onChange={this.handleChange}></TextField>
            <p></p>
            <Button onClick={this.handleSubmit} variant="contained" color="primary" size="medium">Salvar</Button>
        </FormControl>
        )
    }
}