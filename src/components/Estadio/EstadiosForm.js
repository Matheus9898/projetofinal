import React from 'react'
import {Button, TextField, FormControl} from '@material-ui/core'

export default class EstadiosForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            capacidade: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const estadio = {
            nome: this.state.nome,
            capacidade: this.state.capacidade
        }

        this.props.handlePost(estadio)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return ( 
        <FormControl>
            <h2>Estadios</h2>
            <TextField type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></TextField>
            <TextField type="number" id="capacidade" placeholder="Capacidade" value={this.state.capacidade} onChange={this.handleChange}></TextField>
            <p></p>
            <Button onClick={this.handleSubmit} variant="contained" color="primary" size="medium">Salvar</Button>
        </FormControl>
        )
    }
}