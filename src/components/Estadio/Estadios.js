import React from 'react'
import Axios from 'axios'
import EstadiosForm from './EstadiosForm'
import {Table, TableHead, TableBody, TableCell, TableRow, Box, Button} from '@material-ui/core'

export default class Estadios extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            estadios: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8888/estadios/'
        Axios.get(url).then((response) => {
            console.log(response)
            this.setState({
                estadios:response.data
            })
        })
     }

    handlePost = (estadio) => {
        const url = 'http://localhost:8888/estadios/'
        var request = Axios.post(url, estadio)

        request.then((response) => {
            this.setState({
                estadios: [...this.state.estadios, estadio]
            })
            this.handleGet()
        })
    }

    handlePut = (id, estadio) => {
        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:8888/estadios/' + id
        var request = Axios.put(url, estadio)

        request.then((response) => {
            this.handleGet()
        })
    }
    
    handleDelete = (id) => {
        const url = 'http://localhost:8888/estadios/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })           
    }
    
    handleSubmit = (estadio) => {
        if(this.state.selecionado == null){
            this.handlePost(estadio)
        } else {
            this.handlePut(this.state.selecionado, estadio)
        }
    }

    handleSelect = (item) => {
        this.setState({
            selecionado: item._id
        })
    }

    render(){

        var listaEstadios = this.state.estadios.map((item, _id) => {
            
            return(
                    <TableRow flexDirection='column' display="flex" style={{margin: '3rem'}} key={item._id}>
                        <TableCell>{_id + 1}</TableCell>
                        <TableCell>{item.nome}</TableCell>
                        <TableCell>{item.capacidade}</TableCell>
                        <TableCell><Button onClick={() => this.handleSelect(item)} variant="contained" color="primary" size="small">Editar</Button></TableCell>
                        <TableCell><Button onClick={() => this.handleDelete(item._id)} variant="contained" color="primary" size="small">Deletar</Button></TableCell>
                    </TableRow>
            )
        })
        
        return(
            <Box display="flex" flexDirection='column' style={{margin: '3rem'}}>
                <EstadiosForm handlePost={this.handleSubmit}></EstadiosForm>
                <Table>                       
                    <TableHead>
                        <TableRow>
                            <TableCell>Quantidade</TableCell>
                            <TableCell>Nome</TableCell>
                            <TableCell>Capacidade</TableCell>
                            <TableCell>Editar</TableCell>
                            <TableCell>Excluir</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {listaEstadios}
                    </TableBody>
                </Table> 
            </Box>
        )
    }
}