import React from 'react';
import Menu from './components/Menu';
import Jogadores from "./components/Jogador/Jogadores";
import Campeonatos from "./components/Campeonato/Campeonatos";
import Estadios from "./components/Estadio/Estadios";
import Tecnicos from "./components/Tecnico/Tecnicos";
import Times from "./components/Time/Times";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container, Box } from "@material-ui/core";

function App() {
  return (
    <Box>
      <Menu></Menu>
        <Container>
        <BrowserRouter>
          <Switch>
            <Route path='/jogadores' component={Jogadores}></Route>
            <Route path='/campeonatos' component={Campeonatos}></Route>
            <Route path='/estadios' component={Estadios}></Route>
            <Route path='/tecnicos' component={Tecnicos}></Route>
            <Route path='/times' component={Times}></Route>
          </Switch>
        </BrowserRouter>
        </Container>      
    </Box>
  );
}

export default App;
